<?php

ini_set('memory_limit', '-1');
date_default_timezone_set('Europe/Istanbul');
setlocale(LC_ALL, "tr_TR");

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require_once dirname(dirname(__FILE__)) . "/library/tuupolaBasic/autoload.php";


$app = new \Slim\App;

//Routes ...
require "../src/routes/apiUsers.php";
require "../src/config/db.php";

$app->run();

?>