<?php
    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;
    
    require_once dirname(dirname(dirname(__FILE__))) . "/controllers/apiUsers.php";

    $config = [
        'settings' => [
            'addContentLengthHeader' => false,
            'displayErrorDetails' => true
        ]
    ];

    $app = new \Slim\App($config);

    //Basic Authentication
     $app->add(new \Slim\Middleware\HttpBasicAuthentication([
        "secure" => true,
        "realm" => "Protected",
        "authenticator" => function ($arguments) {
            $user = getApiUser($arguments["user"], $arguments["password"]);      
            return $user;
        },
    ]));  

    //Filter
    $app->get('/apiuser/{id}', function (Request $request, Response $response, array $args) {
        $db = new objDB();
        try {
            $id = $request->getAttribute("id");
            $db = $db->connect();
            $sQuery = "SELECT * FROM apiUsers WHERE ID = $id";
            $result = $db->query($sQuery)->fetch(PDO::FETCH_OBJ);
            //if (!empty($result)) {
            return $response->withStatus(200)->withHeader("Content-Type", "application/json")->withJson($result);
            //}
        } catch (PDOException $e) {
            return $response->withJson(
                array(
                    "error" => array(
                        "text" => $e->getMessage(),
                        "code" => $e->getCode()
                    )
                )
            );
        } 
        $db = null;
    });

    $app->get('/loginuser/{tcno}/{pwd}', function (Request $request, Response $response, array $args) {
        $db = new objDB();
        try {
            $tcno = $request->getAttribute("tcno");
            $pwd = $request->getAttribute("pwd");
            $db = $db->connect();
            $sQuery = "SELECT * FROM apiUsers WHERE tcno = '$tcno' and password = '$pwd' ";
            $result = $db->query($sQuery)->fetch(PDO::FETCH_OBJ);
            //if (!empty($result)) {
            return $response->withStatus(200)->withHeader("Content-Type", "application/json")->withJson($result);
            //}
        } catch (PDOException $e) {
            return $response->withJson(
                array(
                    "error" => array(
                        "text" => $e->getMessage(),
                        "code" => $e->getCode()
                    )
                )
            );
        } 
        $db = null;
    });    

    /*

    //List
    $app->get('/courses', function (Request $request, Response $response, array $args) {
        $db = new db();
        try {
            $db = $db->connect();
            $sQuery = "SELECT ID, code, courseName FROM courses";
            $result = $db->query($sQuery)->fetchAll(PDO::FETCH_OBJ);
            //if (!empty($result)) {
            return $response->withStatus(200)->withHeader("Content-Type", "application/json")->withJson($result);
            //}
        } catch (PDOException $e) {
            return $response->withJson(
                array(
                    "error" => array(
                        "text" => $e->getMessage(),
                        "code" => $e->getCode()
                    )
                )
            );
        } 
        $db = null;
    });

    //Add
    $app->post('/course/add', function (Request $request, Response $response, array $args) {
        
        $courseName = $request->getParam("courseName");
        $courseCode = $request->getParam("courseCode");

        $db = new db();
        try {
            $id = $request->getAttribute("id");
            $db = $db->connect();
            $iQuery = "Insert into courses(courseName, courseCode) values(:courseName, :courseCode);";
            $prepare = $db->prepare($iQuery);
            
            $prepare->bindParam("courseName", $courseName);
            $prepare->bindParam("courseCode", $courseCode);

            $result = $prepare->execute();

            if ($result) {
                return $response->withStatus(200)->withHeader("Content-Type", "application/json")->withJson(
                    array (
                        "text" => "Ekleme Basarili"
                    )                
                );
            }
            else {
                return $response->withStatus(500)->withHeader("Content-Type", "application/json")->withJson(
                    array (
                        "error" => array(
                            "text" => "Ekleme Hatasi"
                        )

                    )
                );
            }

        } catch (PDOException $e) {
            return $response->withJson(
                array(
                    "error" => array(
                        "text" => $e->getMessage(),
                        "code" => $e->getCode()
                    )
                )
            );
        } 
        $db = null;
    });

    //Update
    $app->put('/course/update/{id}', function (Request $request, Response $response, array $args) {
        
        $id = $request->getAttribute("id");

        if ($id) {
            $courseName = $request->getParam("courseName");
            $courseCode = $request->getParam("courseCode");

            $db = new db();
            try {
                $id = $request->getAttribute("id");
                $db = $db->connect();
                $iQuery = "Update courses  set courseName=:courseName, courseCode=:courseCode Where ID=$id;";
                $prepare = $db->prepare($iQuery);
                
                $prepare->bindParam("courseName", $courseName);
                $prepare->bindParam("courseCode", $courseCode);
        
                $result = $prepare->execute();
        
                if ($result) {
                    return $response->withStatus(200)->withHeader("Content-Type", "application/json")->withJson(
                        array (
                            "text" => "Guncelleme Basarili"
                        )                
                    );
                }
                else {
                    return $response->withStatus(500)->withHeader("Content-Type", "application/json")->withJson(
                        array (
                            "error" => array(
                                "text" => "Guncelleme Hatasi"
                            )
        
                        )
                    );
                }
        
            } catch (PDOException $e) {
                return $response->withJson(
                    array(
                        "error" => array(
                            "text" => $e->getMessage(),
                            "code" => $e->getCode()
                        )
                    )
                );
            } 

        }
        else {
            return $response->withStatus(500)->withHeader("Content-Type", "application/json")->withJson(
                array (
                    "error" => "Veri Eksik"
                )                
            );
        }

        $db = null;
    });

    //Delete
    $app->delete('/course/{id}', function (Request $request, Response $response, array $args) {
        
        $id = $request->getAttribute("id");

        if ($id) {

            $db = new db();
            try {
                $id = $request->getAttribute("id");
                $db = $db->connect();
                $iQuery = "Delete From courses Where ID = :id;";
                $prepare = $db->prepare($iQuery);

                $prepare->bindParam("id", $id);            
                
                $result = $prepare->execute();
        
                if ($result) {
                    return $response->withStatus(200)->withHeader("Content-Type", "application/json")->withJson(
                        array (
                            "text" => "Silme Basarili"
                        )                
                    );
                }
                else {
                    return $response->withStatus(500)->withHeader("Content-Type", "application/json")->withJson(
                        array (
                            "error" => array(
                                "text" => "Silme Hatasi"
                            )
        
                        )
                    );
                }
        
            } catch (PDOException $e) {
                return $response->withJson(
                    array(
                        "error" => array(
                            "text" => $e->getMessage(),
                            "code" => $e->getCode()
                        )
                    )
                );
            } 

        }
        else {
            return $response->withStatus(500)->withHeader("Content-Type", "application/json")->withJson(
                array (
                    "error" => "Veri Eksik"
                )                
            );
        }

        $db = null;
    });
    */
?>